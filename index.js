// @ts-check

const rms = require('rms-ts')
const Progress = require('cli-progress')
const fs = require('fs')
const credentials = require('./rms-credentials')

const bar = new Progress.Bar({
  format:
    '[{bar}] {percentage}% | ETA: {eta}s | {value}/{total} | Errors: {errors}'
})
let counter = 0

async function work() {
  const lines = fs
    .readFileSync('../zmiany.csv')
    .toString()
    .split('\r\n')

  if (lines.length < 2) {
    throw new Error('Niepoprawny format pliku.')
  }

  const changes = lines.slice(1, lines.length - 1).map(line => {
    const [Id, PlanowanaDataOdsprzedazy] = line.split(';')
    return { Id, PlanowanaDataOdsprzedazy }
  })

  await rms.signIn(credentials)

  const collection = rms.collection('SamochodDTO')

  bar.start(changes.length, counter, {
    errors: 0
  })

  const errors = []

  for (const item of changes) {
    const { PlanowanaDataOdsprzedazy } = item
    try {
      const car = collection.doc(parseInt(item.Id))
      const { NumerRejestracyjny } = await car.get()
      // @ts-ignore
      item.blachy = NumerRejestracyjny
      await car.update({ PlanowanaDataOdsprzedazy })
    } catch (error) {
      // @ts-ignore
      errors.push(`${item.blachy} ${error.message}`)
      bar.update(bar.value, { errors: errors.length })
    }
    bar.update(++counter)
  }

  bar.stop()
  console.log()
  errors.forEach(error => console.log(error))
}

work()
